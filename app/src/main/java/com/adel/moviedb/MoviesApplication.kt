package com.adel.moviedb

import android.app.Application
import com.adel.moviedb.di.component.AppComponent
import com.adel.moviedb.di.component.DaggerAppComponent
import com.adel.moviedb.di.modules.AppModule

/**
 * Created by Ahmed Adel on 30/09/2019.
 * email : a3adel@hotmail.com
 */
class MoviesApplication : Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            // list of modules that are part of this component need to be created here too
            .appModule(AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
            .build();
    }

    override fun onCreate() {
        super.onCreate()
    }


}