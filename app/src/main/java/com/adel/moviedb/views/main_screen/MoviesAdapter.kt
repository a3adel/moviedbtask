package com.adel.moviedb.views.main_screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adel.moviedb.R
import com.adel.moviedb.data.models.Movie
import com.adel.moviedb.utils.OnRvClickListener
import com.adel.moviedb.utils.lazyLoad
import kotlinx.android.synthetic.main.item_movie.view.*

/**
 * Created by Ahmed Adel on 01/10/2019.
 * email : a3adel@hotmail.com
 */
class MoviesAdapter constructor(onRvClickListener: OnRvClickListener) :
    RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>() {
    var movies: ArrayList<Movie>
    lateinit var onRvClickListener: OnRvClickListener

    init {
        movies = ArrayList()
        this.onRvClickListener = onRvClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onRvClickListener.onItemClicked(position)
            }
        })
        holder.itemView.tv_movie_title.text = movies.get(position).title
        holder.itemView.iv_movie_poster.lazyLoad(movies.get(position).getPosterUrl(500))

    }

    class MovieViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!)
}