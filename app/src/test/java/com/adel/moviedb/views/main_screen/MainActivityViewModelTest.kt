package com.adel.moviedb.views.main_screen

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.adel.moviedb.data.DataManager
import com.adel.moviedb.data.api.MoviesDBAPIService
import com.adel.moviedb.data.models.Movie
import com.adel.moviedb.data.models.MoviesResponse
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


/**
 * Created by Ahmed Adel on 02/10/2019.
 * email : a3adel@hotmail.com
 */

@RunWith(MockitoJUnitRunner::class)
class MainActivityViewModelTest {
    lateinit var SUT: MainActivityViewModel
    @get:Rule
    val rule = InstantTaskExecutorRule()


    lateinit var moviesDBAPIService: MoviesDBAPIService
    lateinit var dataManager: DataManager
    @Before
    fun setup() {
        moviesDBAPIService = MoviesDBAPIService.Creator.newAPIService()
        dataManager = DataManager(moviesDBAPIService)
        SUT = MainActivityViewModel(dataManager)
    }

    @Test
    fun getMovies_is_syncing_true() {
        SUT.getMovies(1)
        assert(SUT.isSyncing)
    }

    @Mock
    var movies = Mockito.mock(MoviesResponse::class.java)
    @Mock
    var mMovie1 = Mockito.mock(Movie::class.java)
    @Mock
    var mMovie2 = Mockito.mock(Movie::class.java)
    @Mock
    var mMovie3 = Mockito.mock(Movie::class.java)
    @Mock
    var mMovie4 = Mockito.mock(Movie::class.java)
    @Mock
    var mMovie5 = Mockito.mock(Movie::class.java)
    @Mock
    var mMovie6 = Mockito.mock(Movie::class.java)

    @Test
    fun onMoviesLoaded_updatesMovieList() {
        var moviesList = ArrayList<Movie>()
        moviesList.add(mMovie1)
        moviesList.add(mMovie2)
        moviesList.add(mMovie3)
        movies.results = moviesList

        SUT.updateMoviesList(movies)
        Assert.assertEquals(SUT.moviesList.value, movies.results)
    }

    @Test
    fun movies_filtered_by_yeare_returns_movies_in_year() {
        var moviesList = ArrayList<Movie>()

        mMovie1.releaseDateString = "2016-10-11"
        mMovie2.releaseDateString = "2016-11-11"
        mMovie3.releaseDateString = "2015-10-11"
        mMovie4.releaseDateString = "2013-10-11"
        mMovie5.releaseDateString = "2016-07-11"
        mMovie6.releaseDateString = "2012-09-11"
        moviesList.add(mMovie1)
        moviesList.add(mMovie2)
        moviesList.add(mMovie3)
        moviesList.add(mMovie4)
        moviesList.add(mMovie5)
        moviesList.add(mMovie6)
        Assert.assertEquals(SUT.filterMoviesByDate("2016", moviesList).size, 3)
    }
    
}