package com.adel.moviedb.views

/**
 * Created by Ahmed Adel on 30/09/2019.
 * email : a3adel@hotmail.com
 */
interface IBaseActivity {
    fun showToast(message: String)
    fun showToast(stringId: Int)
    fun isConnectedToNetwork(): Boolean
}