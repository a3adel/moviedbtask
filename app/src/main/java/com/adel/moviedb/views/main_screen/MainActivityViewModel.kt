package com.adel.moviedb.views.main_screen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adel.moviedb.data.DataManager
import com.adel.moviedb.data.models.Movie
import com.adel.moviedb.data.models.MoviesResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject

/**
 * Created by Ahmed Adel on 01/10/2019.
 * email : a3adel@hotmail.com
 */
class MainActivityViewModel @Inject constructor(dataManager: DataManager) : ViewModel() {
    var mDataManger: DataManager
    var moviesList: MutableLiveData<List<Movie>>
    var page = 1
    var isSyncing = false
    lateinit var disposable: Disposable

    init {
        mDataManger = dataManager
        moviesList = MutableLiveData()
    }

    fun getMovies(page: Int) {
        isSyncing = true
        disposable = mDataManger.getMovies(page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::onMoviesLoaded, this::onFailure)
    }

    fun onMoviesLoaded(response: Result<MoviesResponse>) {
        isSyncing = false
        if (response.response().isSuccessful) {
            updateMoviesList(response.response().body()!!)
        }
    }


     fun updateMoviesList(movies: MoviesResponse) {
        moviesList.value = movies?.results
        page++
    }

    private fun onFailure(throwable: Throwable) {
        isSyncing = false
    }
    /**
    didn't have time to implement UI for the filter but you can check the test function for it in
    MainActivityViewModelTest
     */
    fun filterMoviesByDate(year:String,movies:ArrayList<Movie>):List<Movie>{

       return movies?.filter { movie -> movie.releaseDateString.contains(year) }!!
    }
}