This project is a task that fetches the movies from a remote server and loads them to the user and it has the following specs:-
- Based on MVVM architecutre & LiveData
- written with Kotlin 
------------------------------------------------------------------------------------------------------------------------------
It has the following features :-
- fetching the movies from the server 
- infinite scrolling on the recycler view
- once clicking on any movie it opens the movie screen
- smal unit tests using Esporesso, JUnit4 and Mockito
------------------------------------------------------------------------------------------------------------------------------
it uses the following libraries :-
- Retrofit for networking 
- RxJava
- Picasso for images lazy loading 
- Dagger2 for injection
- android Jetpack 