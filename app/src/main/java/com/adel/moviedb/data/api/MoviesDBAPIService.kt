package com.adel.moviedb.data.api

import com.adel.moviedb.data.models.MoviesResponse
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.Result
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

/**
 * Created by Ahmed Adel on 30/09/2019.
 * email : a3adel@hotmail.com
 */
 interface MoviesDBAPIService {

    @GET("3/discover/movie?api_key=166d823124931341b391456bdfe81f76")
     fun discoverMovies(@Query("page") page: Int): Single<Result<MoviesResponse>>

    public class Creator {
        companion object {
            fun newAPIService()
                    : MoviesDBAPIService {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(40, TimeUnit.SECONDS)
                    .addInterceptor(httpLoggingInterceptor).build()
                val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("https://api.themoviedb.org")
                    .client(okHttpClient)
                    .build();
                return retrofit.create(MoviesDBAPIService::class.java)
            }
        }
    }
}