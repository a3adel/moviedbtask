package com.adel.moviedb.di.modules

import android.app.Application
import com.adel.moviedb.data.api.MoviesDBAPIService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Ahmed Adel on 30/09/2019.
 * email : a3adel@hotmail.com
 */
@Module
class AppModule constructor(application: Application) {
    var myApplication: Application

    init {
        myApplication = application
    }

    @Provides
    @Singleton
    fun providesApplication(): Application {
        return myApplication
    }

    @Provides
    @Singleton
    fun provideAPIServices(): MoviesDBAPIService {
        return MoviesDBAPIService.Creator.newAPIService()
    }


}