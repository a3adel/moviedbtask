package com.adel.moviedb.di.component

import com.adel.moviedb.di.modules.AppModule
import com.adel.moviedb.di.modules.ReposModule
import com.adel.moviedb.di.modules.ViewModelsModule
import com.adel.moviedb.views.main_screen.MainActivity
import com.adel.moviedb.views.MovieDetailsActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Ahmed Adel on 30/09/2019.
 * email : a3adel@hotmail.com
 */
@Singleton
@Component(modules = [AppModule::class, ReposModule::class, ViewModelsModule::class])
interface AppComponent {
    fun inject(mainActivity: MainActivity)

    fun inject(movieDetailsActivity: MovieDetailsActivity)
}