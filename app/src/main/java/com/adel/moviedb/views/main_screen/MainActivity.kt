package com.adel.moviedb.views.main_screen

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.adel.moviedb.MoviesApplication
import com.adel.moviedb.R
import com.adel.moviedb.di.MoviesDBViewModelProvider
import com.adel.moviedb.utils.OnRvClickListener
import com.adel.moviedb.utils.OnScrollListener
import com.adel.moviedb.views.BaseActivity
import com.adel.moviedb.views.MovieDetailsActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelProvider: MoviesDBViewModelProvider
    val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this, viewModelProvider)[MainActivityViewModel::class.java]
    }
    val moviesAdapter: MoviesAdapter by lazy {
        MoviesAdapter(object : OnRvClickListener {
            override fun onItemClicked(position: Int) {
                val intent = Intent(this@MainActivity, MovieDetailsActivity::class.java)
                intent.putExtra(MovieDetailsActivity.MOVIE_DETAILS_KEY, moviesAdapter.movies.get(position))
                startActivity(intent)
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as MoviesApplication).appComponent.inject(this)
        initMoviesRv()
        viewModel.moviesList.observe(this, androidx.lifecycle.Observer {
            moviesAdapter.movies.addAll(it)
            moviesAdapter.notifyDataSetChanged()
            pb_loading.visibility = View.GONE

        })
        pb_loading.visibility = View.VISIBLE
        viewModel.getMovies(viewModel.page)
    }

    private fun initMoviesRv() {
        val layoutManager = LinearLayoutManager(this)
        rv_movies.layoutManager = layoutManager
        rv_movies.adapter = moviesAdapter
        rv_movies.OnScrollListener(){
            if (layoutManager.findLastVisibleItemPosition() == moviesAdapter.movies.size - 1
                && !viewModel.isSyncing
                && isConnectedToNetwork()
            ) {
                pb_loading.visibility = View.VISIBLE

                viewModel.getMovies(viewModel.page)
            }
        }

    }


}
