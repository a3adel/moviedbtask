package com.adel.moviedb.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Ahmed Adel on 01/10/2019.
 * email : a3adel@hotmail.com
 */
open class Movie(
    @SerializedName("popularity") var popularity: Float,
    @SerializedName("vote_count") var voteCount: Int,
    @SerializedName("video") var hasVideo: Boolean,
    @SerializedName("poster_path") var posterPath: String,
    @SerializedName("id") var id: Long,
    @SerializedName("adult") var isAdultMovie: Boolean,
    @SerializedName("backdrop_path") var backdropPath: String,
    @SerializedName("original_language") var language: String,
    @SerializedName("original_title") var originalTitle: String,
    @SerializedName("title") var title: String,
    @SerializedName("vote_average") var voteAverage: Float,
    @SerializedName("overview") var overview: String,
    @SerializedName("release_date") internal var releaseDateString: String
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readFloat(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
    parcel.readString(),
        parcel.readFloat(),
        parcel.readString(),
        parcel.readString()
    ) {
    }
    fun getPosterUrl(screenWidth: Int): String {
        return "https://image.tmdb.org/t/p/w$screenWidth$posterPath"

    }

    fun getReleaseDate():Date{

        return SimpleDateFormat("yyyy-MM-d").parse(releaseDateString)
    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(popularity)
        parcel.writeInt(voteCount)
        parcel.writeByte(if (hasVideo) 1 else 0)
        parcel.writeString(posterPath)
        parcel.writeLong(id)
        parcel.writeByte(if (isAdultMovie) 1 else 0)
        parcel.writeString(backdropPath)
        parcel.writeString(language)
        parcel.writeString(originalTitle)
        parcel.writeString(title)
        parcel.writeFloat(voteAverage)
        parcel.writeString(overview)
        parcel.writeString(releaseDateString)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }
    }
}