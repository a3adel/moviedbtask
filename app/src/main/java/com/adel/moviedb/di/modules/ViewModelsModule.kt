package com.adel.moviedb.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.adel.moviedb.di.MoviesDBViewModelProvider
import com.adel.moviedb.di.ViewModelKey
import com.adel.moviedb.views.main_screen.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Ahmed Adel on 30/09/2019.
 * email : a3adel@hotmail.com
 */
@Module
abstract class ViewModelsModule {
    @Binds
    internal abstract fun bindViewModelFactory(viewModelFactory: MoviesDBViewModelProvider): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun mainActivityViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel
}