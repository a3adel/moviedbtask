package com.adel.moviedb.data.models

import com.google.gson.annotations.SerializedName

open  class MoviesResponse(@SerializedName("page")var page:String,
                          @SerializedName("total_results")var totalResults:Int,
                          @SerializedName("total_pages")var totalPages:Int,
                          @SerializedName("results")var results:List<Movie>) {

}
