package com.adel.moviedb.data

import com.adel.moviedb.data.api.MoviesDBAPIService
import com.adel.moviedb.data.models.MoviesResponse
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject

/**
 * Created by Ahmed Adel on 01/10/2019.
 * email : a3adel@hotmail.com
 */
class DataManager @Inject constructor(moviesDBAPIService: MoviesDBAPIService) {
    var mMoviesDBAPIService: MoviesDBAPIService

    init {
        mMoviesDBAPIService = moviesDBAPIService
    }

    fun getMovies(page: Int): Single<Result<MoviesResponse>> {
        if (page > 0)
            return mMoviesDBAPIService.discoverMovies(page)
        throw IllegalArgumentException("Page count must be bigger than 0")
    }
}