package com.adel.moviedb.data

import com.adel.moviedb.data.api.MoviesDBAPIService
import org.junit.Before
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Ahmed Adel on 02/10/2019.
 * email : a3adel@hotmail.com
 */

@RunWith(MockitoJUnitRunner::class)
class DataManagerTest {
    lateinit var SUT: DataManager
    lateinit var moviesDBAPIService: MoviesDBAPIService
    public var expectedException= ExpectedException.none()
    @Before
    fun setup() {
        moviesDBAPIService = MoviesDBAPIService.Creator.newAPIService()
        SUT = DataManager(moviesDBAPIService)

    }

    @Test(expected = IllegalArgumentException::class)
    fun getMovies_throw_Excption_invalid_page_number() {
        // Arrange
        // Act
         SUT.getMovies(0)
        // Assert
    }
}