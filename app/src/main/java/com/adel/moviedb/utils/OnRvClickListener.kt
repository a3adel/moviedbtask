package com.adel.moviedb.utils

/**
 * Created by Ahmed Adel on 02/10/2019.
 * email : a3adel@hotmail.com
 */
interface OnRvClickListener {
    fun onItemClicked(position:Int)
}