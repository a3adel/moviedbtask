package com.adel.moviedb.views

import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.runner.AndroidJUnit4
import com.adel.moviedb.R
import com.adel.moviedb.data.models.Movie
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by Ahmed Adel on 02/10/2019.
 * email : a3adel@hotmail.com
 */

@RunWith(AndroidJUnit4::class)
class MovieDetailsActivityTest {
    lateinit var SUT: MovieDetailsActivity
    lateinit var movie: Movie
    @get:Rule var activityScenarioRule = activityScenarioRule<MovieDetailsActivity>()


    @Before
    fun setup() {

    }

    @Test
    fun verify_overview_textview_contains_movie_overivew() {
        // Arrange
        movie=Movie(9.8f,2,false,"asd",987,true,"asds","en","harry","harry",9.3f,"asdf","")

        activityScenarioRule.scenario.onActivity { activity -> activity.initViews(Movie(9.8f,2,false,"asd",987,true,"asds","en","harry","harry",9.3f,"asdf","")) }
        Espresso.onView(ViewMatchers.withId(R.id.tv_overview))
            .check(ViewAssertions.matches(ViewMatchers.withText(movie.overview)))

    }
}