package com.adel.moviedb.views

import android.os.Bundle
import com.adel.moviedb.R
import com.adel.moviedb.data.models.Movie
import com.adel.moviedb.utils.lazyLoad
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : BaseActivity() {

    companion object {
        val MOVIE_DETAILS_KEY = "movieDetailsActivity_movieDetails"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        val movie = intent.getParcelableExtra<Movie>(MOVIE_DETAILS_KEY)
        if (movie != null)
            initViews(movie)
    }

    fun initViews(movie: Movie) {
        tv_overview.text = movie.overview
        tv_movie_details_title.text = movie.title
        iv_small_poster.lazyLoad(movie.getPosterUrl(200))
        rating_movie.rating = movie.voteAverage / 2
    }
}
