package com.adel.moviedb.utils

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

/**
 * Created by Ahmed Adel on 04/10/2019.
 * email : a3adel@hotmail.com
 */
fun ImageView.lazyLoad(url:String){
    Picasso.get().load(url).into(this)
}

fun RecyclerView.OnScrollListener(onAction:()-> Unit){
    this.addOnScrollListener(object: RecyclerView.OnScrollListener(){
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            onAction.invoke()
        }
    })
}